require_relative "node"

module BbcodeParser
  def self.parse(str)
    tokens = []
    current_cursor = :value # :value or :argument
    current_node = nil   # nil or Node

    str.each_char do |c|
      current_cursor = :value if !current_node.nil? and current_node.is_text?
      case c
      when "["
        tokens.push(current_node.serialize) if !current_node.nil? and current_node.is_text?
        current_node = Node.new :tag
      when "]"
        tokens.push(current_node.serialize) if !current_node.nil? and current_node.is_tag?
        current_node = Node.new :text
      when "/"
        current_node.tag_type = :close_tag if !current_node.nil? and current_node.is_tag?
      when "="
        current_cursor = :argument if !current_node.nil? and current_node.is_tag?
      else
        current_node = Node.new :text if current_node.nil?
        current_node.push c, current_cursor
      end
    end

    tokens.delete_if {|value| value.nil? }

    return tokens
  end
end
