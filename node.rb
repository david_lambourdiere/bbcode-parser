module BbcodeParser
  class Node

    attr_accessor :tag_type, :argument, :value

    def initialize(type)
      @type = type # :tag or :text
      @tag_type = :open_tag if self.is_tag?
      @value = @argument = nil
    end

    def is_tag?
      @type == :tag
    end

    def is_text?
      @type == :text
    end

    def push(str, cursor)
      if cursor == :value
        @value = String.new if @value.nil?
        @value += str
      elsif cursor == :argument
        @argument = String.new if @argument.nil?
        @argument += str
      end
    end

    def serialize
      if self.is_tag?
        return {@tag_type => @value, :argument => @argument } if @tag_type == :open_tag
        return {@tag_type => @value} if @tag_type == :close_tag
      end

      return {:text => @value} if self.is_text? and !@value.nil? and !@value.empty?
    end
  end
end
